package com.zuitt;

import java.io.IOException;
//import java.io.PrintWriter;

//import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	public void init() throws ServletException{
		System.out.println("**********************");
		System.out.println("UserServlet has been initialized.");
		System.out.println("**********************");
	}
		public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
			String firstName=req.getParameter("firstname");
			String lastName=req.getParameter("lastname");
			String Email=req.getParameter("email");
			String Contactnumber=req.getParameter("contactnumber");
			
			//SystemProperties
			System.getProperties().put("first_name", firstName);
			//HttpSession
			HttpSession sess=req.getSession();
			sess.setAttribute("lastname",lastName);
			
			
			//Servlet Context setAttribute method
			ServletContext srvContext = getServletContext();
			srvContext.setAttribute("email", Email);
			
			//Url Rewriting via sendRedirect method
			res.sendRedirect("details?contactnumber="+Contactnumber);		
			//RequestDispatcher rd= req.getRequestDispatcher("details");
			//rd.forward(req,res);

		}
	public void destroy() {
			System.out.println("**********************");
			System.out.println("UserServlet has been destroy.");
			System.out.println("**********************");
		}

}
