package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	public void init() throws ServletException{
		System.out.println("**********************");
		System.out.println("DetailsServlet has been initialized.");
		System.out.println("**********************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		ServletContext srvContext=getServletContext();
		//String firstName=req.getParameter("firstname");
		//String lastName=req.getParameter("lastname");
		//String Email=req.getParameter("email");
		
		
		
		String Branding= srvContext.getInitParameter("branding");
		
		//System Parameters
		String firstname= System.getProperty("first_name");
		
		//HttpSession
		HttpSession sess=req.getSession();
		String lastname= sess.getAttribute("lastname").toString();
		
		//ServletContext
		String Email= srvContext.getAttribute("email").toString();
		
		//SendRedirect
		String Contactnumber=req.getParameter("contactnumber");
		
		
		PrintWriter out= res.getWriter();
		out.println(
				"<h1>"+Branding+"</h1>"+
				"<p>First Name: "+firstname+"</p>"+
				"<p>Last Name: "+lastname+"</p>"+
				"<p>Contact: "+Contactnumber+"</p>"+
				"<p>Email: "+Email+"</p>"
		);

	}
	
	public void destroy() {
		System.out.println("**********************");
		System.out.println("DetailsServlet has been destroy.");
		System.out.println("**********************");
	}

	
	
}
